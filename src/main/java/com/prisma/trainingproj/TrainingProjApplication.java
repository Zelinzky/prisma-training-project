package com.prisma.trainingproj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingProjApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrainingProjApplication.class, args);
    }

}
